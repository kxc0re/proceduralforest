//
// Created by k3ksc on 09/02/2018.
//

#ifndef PROCEDURALFOREST_RESOURCECONTAINER_H
#define PROCEDURALFOREST_RESOURCECONTAINER_H


#include <ostream>
#include <utility>

#include "resources/Resource.h"

struct ResourceContainer {

    Water water;
    Light light;
    Carbon carbon;
    Nitrogen nitrogen;
    Phosphor phosphor;

    ResourceContainer(Water water, Light light, Carbon carbon, Nitrogen nitrogen, Phosphor phosphor);

    ResourceContainer() = default;

    bool meetsDemand(const ResourceContainer& demand) const {
        return water >= demand.water
               && light >= demand.light
               && carbon >= demand.carbon
               && nitrogen >= demand.nitrogen
               && phosphor >= demand.phosphor;
    }

    Water getWater() const;

    Light getLight() const;

    Carbon getCarbon() const;

    Nitrogen getNitrogen() const;

    Phosphor getPhosphor() const;

    ResourceContainer& operator+=(const ResourceContainer& rhs);

    ResourceContainer& operator-=(const ResourceContainer& rhs);

    ResourceContainer& operator*=(const ResourceContainer& rhs);

    ResourceContainer& operator/=(const ResourceContainer& rhs);


    ResourceContainer& operator+=(const Water& rhs);

    ResourceContainer& operator-=(const Water& rhs);

    ResourceContainer& operator+=(const Light& rhs);

    ResourceContainer& operator-=(const Light& rhs);

    ResourceContainer& operator+=(const Carbon& rhs);

    ResourceContainer& operator-=(const Carbon& rhs);

    ResourceContainer& operator+=(const Phosphor& rhs);

    ResourceContainer& operator-=(const Phosphor& rhs);

    ResourceContainer& operator+=(const Nitrogen& rhs);

    ResourceContainer& operator-=(const Nitrogen& rhs);

    ResourceContainer& operator*=(const float& rhs);

    ResourceContainer& operator/=(const float& rhs);

    friend inline bool operator==(const ResourceContainer& lhs, const ResourceContainer& rhs) {
        return lhs.water == rhs.water
               && lhs.light == rhs.light
               && lhs.carbon == rhs.carbon
               && lhs.nitrogen == rhs.nitrogen
               && lhs.phosphor == rhs.phosphor;
    }

    friend inline std::ostream& operator<<(std::ostream& output, const ResourceContainer& rhs) {
        output << "{"
            << rhs.water << ", "
            << rhs.light << ", "
            << rhs.carbon << ", "
            << rhs.nitrogen << ", "
            << rhs.phosphor
            << "}";

        return output;
    }

    inline bool isNegative() const {
        return water.getValue() < 0.f
               || light.getValue() < 0.f
               || carbon.getValue() < 0.f
               || nitrogen.getValue() < 0.f
               || phosphor.getValue() < 0.f;
    };

    inline bool isInfinite() const {
        return std::isinf(water)
               || std::isinf(light)
               || std::isinf(carbon)
               || std::isinf(nitrogen)
               || std::isinf(phosphor);
    }

    inline bool isInvalid() const {
        return isNegative() || isInfinite();
    }

};

struct ResourceSupply : ResourceContainer {
    using ResourceContainer::ResourceContainer;

    ResourceSupply(ResourceContainer& container)
        : ResourceContainer(container.getWater(), container.getLight(), container.getCarbon(),
                            container.getNitrogen(), container.getPhosphor()) {}

    ResourceSupply() = default;

};

struct ResourceDemand : ResourceContainer {
    using ResourceContainer::ResourceContainer;

    ResourceDemand(ResourceContainer& container)
        : ResourceContainer(container.getWater(), container.getLight(), container.getCarbon(),
                            container.getNitrogen(), container.getPhosphor()) {}

    ResourceDemand() = default;
};

//specific for supply/demand

inline bool operator<(const ResourceSupply& supply, const ResourceDemand& demand) {
    return !supply.meetsDemand(demand);
}

inline bool operator>(const ResourceSupply& supply, const ResourceDemand& demand) {
    return supply.meetsDemand(demand);
}

inline bool operator<(const ResourceDemand& demand, const ResourceSupply& supply) {
    return supply.meetsDemand(demand);
}

inline bool operator>(const ResourceDemand& demand, const ResourceSupply& supply) {
    return !supply.meetsDemand(demand);
}


//common operators
inline ResourceContainer operator+(ResourceContainer lhs, const ResourceContainer& rhs) {
    return lhs += rhs;
}

inline ResourceContainer operator-(ResourceContainer lhs, const ResourceContainer& rhs) {
    return lhs -= rhs;
}

inline ResourceContainer operator*(ResourceContainer lhs, const ResourceContainer& rhs) {
    return lhs *= rhs;
}

inline ResourceContainer operator/(ResourceContainer lhs, const ResourceContainer& rhs) {
    return lhs /= rhs;
}


inline ResourceContainer operator+(ResourceContainer lhs, const Water& rhs) {
    return lhs += rhs;
}

inline ResourceContainer operator-(ResourceContainer lhs, const Water& rhs) {
    return lhs -= rhs;
}

inline ResourceContainer operator+(ResourceContainer lhs, const Light& rhs) {
    return lhs += rhs;
}

inline ResourceContainer operator-(ResourceContainer lhs, const Light& rhs) {
    return lhs -= rhs;
}

inline ResourceContainer operator+(ResourceContainer lhs, const Carbon& rhs) {
    return lhs += rhs;
}

inline ResourceContainer operator-(ResourceContainer lhs, const Carbon& rhs) {
    return lhs -= rhs;
}

inline ResourceContainer operator+(ResourceContainer lhs, const Phosphor& rhs) {
    return lhs += rhs;
}

inline ResourceContainer operator-(ResourceContainer lhs, const Phosphor& rhs) {
    return lhs -= rhs;
}

inline ResourceContainer operator+(ResourceContainer lhs, const Nitrogen& rhs) {
    return lhs += rhs;
}

inline ResourceContainer operator-(ResourceContainer lhs, const Nitrogen& rhs) {
    return lhs -= rhs;
}

inline ResourceContainer operator*(ResourceContainer lhs, const float& rhs) {
    return lhs *= rhs;
}

inline ResourceContainer operator/(ResourceContainer lhs, const float& rhs) {
    return lhs /= rhs;
}


#endif //PROCEDURALFOREST_RESOURCECONTAINER_H
