//
// Created by k3ksc on 11/04/2018.
//

#include <functional>
#include <iostream>
#include "utils/RandomGenerator.h"
#include "utils/CircleHelper.h"
#include "World.h"

void World::createTestGrid() {
    LOG_DEBUG("Creating Test Grid");
    std::normal_distribution<float> resourceDistribution(100.f, 25.f);
    std::normal_distribution<float> waterDistribution(100.f, 5.f);
    std::normal_distribution<float> lightDistribution(100.f, 5.f);

    auto randResources = std::bind(resourceDistribution, 
        std::ref(RandomGenerator::getInstance().getGenerator()));
    auto randWater = std::bind(waterDistribution, 
        std::ref(RandomGenerator::getInstance().getGenerator()));
    auto randLight = std::bind(lightDistribution, 
        std::ref(RandomGenerator::getInstance().getGenerator()));

    for (auto& container : *resourceGrid_) {
        auto carbon = randResources();
        if (carbon < 0.f) { carbon = 0.f; }

        auto nitrogen = randResources();
        if (nitrogen < 0.f) { nitrogen = 0.f; }

        auto phosphor = randResources();
        if (phosphor < 0.f) { phosphor = 0.f; }

        container = { 0.f, 0.f, carbon, nitrogen, phosphor};
    }
}

void World::createTestModifiers() {
    LOG_DEBUG("Creating Test Modifiers");

    const auto sproutingChance = 0.001f;
    auto grow = std::bind(std::bernoulli_distribution(sproutingChance),
                          std::ref(RandomGenerator::getInstance().getGenerator()));
    auto which = std::bind(std::uniform_int_distribution<int>(0, 3),
                           std::ref(RandomGenerator::getInstance().getGenerator()));

    for (auto it = resourceGrid_->begin(); it != resourceGrid_->end(); ++it) {
        const auto position = resourceGrid_->positionFromIterator(it);
        if (position.isInvalid()) { continue; }

        if (grow()) {
            std::unique_ptr<ResourceModifier> modifier;
            const auto w = which();
            switch (w) {
            case 1: {
                modifier = modifierFactory_.createCarbonTree();
                break;
            }
            case 2: {
                modifier = modifierFactory_.createNitrogenTree();
                break;
            }
            case 3: {
                modifier = modifierFactory_.createPhosphorTree();
                break;
            }

            default: {
                modifier = modifierFactory_.createAverageTree();
                break;
            }

            }
            modifierRegistry_.registerModifier(position, std::move(modifier));
        }
    }

    heatmapGenerator_.draw(0);
    treemapGenerator_.draw(0);

}

void World::createTest() {
    RandomGenerator::getInstance().seed(42);

    CircleHelper::getInstance().setCircleFunction(CircleFunction::MIDPOINT);

    createTestGrid();
    createTestModifiers();
}

World::World()
    : resourceGrid_(std::make_unique<grid_type>()),
      modifierRegistry_(resourceGrid_.get(), &modifierFactory_),
      heatmapGenerator_(resourceGrid_.get()),
      treemapGenerator_(&modifierRegistry_) {

    createTest();
}

void World::tick() {
    LOG_DEBUG("Starting Tick " << iteration_);

    modifierRegistry_.update();

    heatmapGenerator_.draw(iteration_);
    treemapGenerator_.draw(iteration_);

    ++iteration_;

}
