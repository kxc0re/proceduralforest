//
// Created by k3ksc on 27/02/2018.
//

#include "Resource.h"

Resource::Resource(const float value) : value_(value) {}

Resource::Resource() : value_(0.f) {}

Resource& Resource::operator+=(const Resource& rhs) {
    value_ += rhs.value_;
    return *this;
}

Resource& Resource::operator-=(const Resource& rhs) {
    value_ -= rhs.value_;
    return *this;
}

Resource& Resource::operator*=(const Resource& rhs) {
    value_ *= rhs.value_;
    return *this;
}

Resource& Resource::operator/=(const Resource& rhs) {
    value_ /= rhs.value_;
    return *this;
}

Resource& Resource::operator*=(const float& rhs) {
    value_ *= rhs;
    return *this;
}

Resource& Resource::operator/=(const float& rhs) {
    value_ /= rhs;
    return *this;
}

float Resource::getValue() const {
    return value_;
}
