//
// Created by k3ksc on 09/02/2018.
//

#ifndef PROCEDURALFOREST_RESOURCE_H
#define PROCEDURALFOREST_RESOURCE_H

struct Resource {
private:
    float value_;

public:
    Resource(float value);

    Resource();

    float getValue() const;

    Resource& operator+=(const Resource& rhs);

    Resource& operator-=(const Resource& rhs);

    Resource& operator*=(const Resource& rhs);

    Resource& operator/=(const Resource& rhs);

    Resource& operator*=(const float& rhs);

    Resource& operator/=(const float& rhs);

    friend bool operator==(const Resource& lhs, const Resource& rhs) {
        return lhs.value_ == rhs.value_;
    }

    friend bool operator<(const Resource& lhs, const Resource& rhs) {
        return lhs.value_ < rhs.value_;
    }

    operator float() const {
        return this->value_;
    }
};

inline Resource operator+(Resource lhs, const Resource& rhs) {
    return lhs += rhs;
}

inline Resource operator-(Resource lhs, const Resource& rhs) {
    return lhs -= rhs;
}

inline Resource operator*(Resource lhs, const float& rhs) {
    return lhs *= rhs;
}

inline Resource operator/(Resource lhs, const float& rhs) {
    return lhs /= rhs;
}

inline bool operator!=(const Resource& lhs, const Resource& rhs) {
    return !operator==(lhs, rhs);
}
inline bool operator>=(const Resource& lhs, const Resource& rhs) {
    return !operator<(lhs, rhs);
}
inline bool operator>(const Resource& lhs, const Resource& rhs) {
    return operator<(rhs, lhs);
}
inline bool operator<=(const Resource& lhs, const Resource& rhs) {
    return !operator<(rhs, lhs);
}

struct Light : Resource {
    Light() = default;

    Light(const float value)
        : Resource(value) {}
};

struct Water : Resource {
    Water() = default;

    Water(const float value)
        : Resource(value) {}
};

struct Carbon : Resource {
    Carbon() = default;

    Carbon(const float value)
        : Resource(value) {}
};

struct Nitrogen : Resource {
    Nitrogen() = default;

    Nitrogen(const float value)
        : Resource(value) {}
};

struct Phosphor : Resource {
    Phosphor() = default;

    Phosphor(const float value)
        : Resource(value) {}
};


#endif //PROCEDURALFOREST_RESOURCE_H
