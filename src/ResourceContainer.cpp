//
// Created by k3ksc on 09/02/2018.
//

#include "ResourceContainer.h"

ResourceContainer::ResourceContainer(Water water, Light light, Carbon carbon, Nitrogen nitrogen, Phosphor phosphor)
    : water(water),
      light(light),
      carbon(carbon),
      nitrogen(nitrogen),
      phosphor(phosphor) {}

 

ResourceContainer& ResourceContainer::operator+=(const ResourceContainer& rhs) {
    water += rhs.water;
    light += rhs.light;
    carbon += rhs.carbon;
    nitrogen += rhs.nitrogen;
    phosphor += rhs.phosphor;

    return *this;
}

ResourceContainer& ResourceContainer::operator-=(const ResourceContainer& rhs) {
    water -= rhs.water;
    light -= rhs.light;
    carbon -= rhs.carbon;
    nitrogen -= rhs.nitrogen;
    phosphor -= rhs.phosphor;

    return *this;
}

ResourceContainer& ResourceContainer::operator*=(const ResourceContainer& rhs) {
    water *= rhs.water;
    light *= rhs.light;
    carbon *= rhs.carbon;
    nitrogen *= rhs.nitrogen;
    phosphor *= rhs.phosphor;

    return *this;
}

ResourceContainer& ResourceContainer::operator/=(const ResourceContainer& rhs) {
    water /= rhs.water;
    light /= rhs.light;
    carbon /= rhs.carbon;
    nitrogen /= rhs.nitrogen;
    phosphor /= rhs.phosphor;

    return *this;
}

ResourceContainer& ResourceContainer::operator+=(const Water& rhs) {
    water += rhs;
    return *this;
}

ResourceContainer& ResourceContainer::operator-=(const Water& rhs) {
    water -= rhs;
    return *this;
}

ResourceContainer& ResourceContainer::operator+=(const Light& rhs) {
    light += rhs;
    return *this;
}

ResourceContainer& ResourceContainer::operator-=(const Light& rhs) {
    light -= rhs;
    return *this;
}

ResourceContainer& ResourceContainer::operator+=(const Carbon& rhs) {
    carbon += rhs;
    return *this;
}

ResourceContainer& ResourceContainer::operator-=(const Carbon& rhs) {
    carbon -= rhs;
    return *this;
}

ResourceContainer& ResourceContainer::operator+=(const Phosphor& rhs) {
    phosphor += rhs;
    return *this;
}

ResourceContainer& ResourceContainer::operator-=(const Phosphor& rhs) {
    phosphor -= rhs;
    return *this;
}

ResourceContainer& ResourceContainer::operator+=(const Nitrogen& rhs) {
    nitrogen += rhs;
    return *this;
}

ResourceContainer& ResourceContainer::operator-=(const Nitrogen& rhs) {
    nitrogen -= rhs;
    return *this;
}

ResourceContainer& ResourceContainer::operator*=(const float& rhs) {
    water *= rhs;
    light *= rhs;
    carbon *= rhs;
    phosphor *= rhs;
    nitrogen *= rhs;

    return *this;
}

ResourceContainer& ResourceContainer::operator/=(const float& rhs) {
    water /= rhs;
    light /= rhs;
    carbon /= rhs;
    phosphor /= rhs;
    nitrogen /= rhs;
    return *this;
}

Water ResourceContainer::getWater() const { return water; }

Light ResourceContainer::getLight() const { return light; }

Carbon ResourceContainer::getCarbon() const { return carbon; }

Nitrogen ResourceContainer::getNitrogen() const { return nitrogen; }

Phosphor ResourceContainer::getPhosphor() const { return phosphor; }
