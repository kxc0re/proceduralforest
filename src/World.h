//
// Created by k3ksc on 11/04/2018.
//

#ifndef PROCEDURALFOREST_WORLD_H
#define PROCEDURALFOREST_WORLD_H


#include "utils/grid.h"
#include "utils/typedefs.h"
#include "utils/heatmap/HeatmapGenerator.h"

#include "modifier/ResourceModifierRegistry.h"
#include "modifier/ResourceModifierFactory.h"


class World {
    typedef grid::grid<ResourceSupply, grid_width, grid_height> grid_type;
    typedef std::unique_ptr<grid_type> grid_ptr;

    grid_ptr resourceGrid_;
    ResourceModifierRegistry modifierRegistry_;
    ResourceModifierFactory modifierFactory_;

    int iteration_ = 1;

    HeatmapGenerator heatmapGenerator_;
    TreemapGenerator treemapGenerator_;


public:

    void createTestGrid();
    void createTestModifiers();
    void createTest();
    World();

    void tick();

};
#endif //PROCEDURALFOREST_WORLD_H
