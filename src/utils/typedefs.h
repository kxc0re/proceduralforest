//
// Created by stenmark on 05.03.2018.
//

#ifndef PROCEDURALFOREST_TYPEDEFS_H
#define PROCEDURALFOREST_TYPEDEFS_H

#include <iostream>

#define LDEBUG
#define LINFO
#define LERROR

#ifdef LDEBUG
#define LOG_DEBUG(X); std::cout<<"[DEB] "<<X<<std::endl;
#else
#define LOG_DEBUG(X);
#endif

#ifdef LINFO
#define LOG_INFO(X); std::cout<<"[INF] "<<X<<std::endl;
#else
#define LOG_INFO(X);
#endif
#ifdef LERROR
#define LOG_ERROR(X); std::cout<<"[ERR] "<<X<<std::endl;
#else
#define LOG_ERROR(X);
#endif

constexpr int grid_width = 1000, grid_height = 1000;


#endif //PROCEDURALFOREST_TYPEDEFS_H
