//
// Created by k3ksc on 13/04/2018.
//

#include "RandomGenerator.h"

RandomGenerator &RandomGenerator::getInstance() {
    static RandomGenerator instance;
    return instance;
}

void RandomGenerator::seed(const unsigned int seed) {
    generator_.seed(seed);
}

std::default_random_engine &RandomGenerator::getGenerator() {
    return generator_;
}
