//
// Created by k3ksc on 13/04/2018.
//

#ifndef PROCEDURALFOREST_RANDOMGENERATOR_H
#define PROCEDURALFOREST_RANDOMGENERATOR_H


#include <random>

class RandomGenerator {

    RandomGenerator() = default;
    ~RandomGenerator() = default;

    std::default_random_engine generator_;

public:
    static RandomGenerator& getInstance();

    std::default_random_engine& getGenerator();

    void seed(unsigned int seed);

    RandomGenerator(RandomGenerator &&) = delete;
    RandomGenerator(RandomGenerator const&) = delete;
    void operator=(RandomGenerator const&) = delete;
    void operator=(RandomGenerator &&) = delete;

};


#endif //PROCEDURALFOREST_RANDOMGENERATOR_H
