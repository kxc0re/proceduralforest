//
// Created by k3ksc on 27/02/2018.
//

#include "CircleHelper.h"
#include <iterator>
#include <algorithm>


void CircleHelper::setCircleFunction(offset_set (CircleHelper::* circleFunction)(const int) const) {
    circleFunction_ = circleFunction;
    getInstance().generateOffsetTable();
}

void CircleHelper::setFactorFunction(float (CircleHelper::*factorFunction)(const int) const) {
    factorFunction_ = factorFunction;

}

CircleHelper& CircleHelper::getInstance(offset_set (CircleHelper::* circleFunction)(const int) const,
                                        float (CircleHelper::*factorFunction)(const int) const) {
    static CircleHelper instance(circleFunction, factorFunction);

    return instance;
}

void CircleHelper::generateOffsetTable(const int radius, const bool overwrite) {
    if (overwrite) {
        offsetTable_.clear();
    }
    for (auto i = static_cast<int>(offsetTable_.size()); i <= radius; ++i) {
        offsetTable_.push_back((this->*circleFunction_)(static_cast<int>(i)));
    }

}

void CircleHelper::setCircleFunction(const CircleFunction fun) const {
    switch (fun) {
    case MIDPOINT: {
        getInstance().setCircleFunction(&CircleHelper::midpointCircle);
        break;
    }
    case SQUARE: {
        getInstance().setCircleFunction(&CircleHelper::square);
        break;
    }
    default: case MANHATTAN: {
        getInstance().setCircleFunction(&CircleHelper::manhattanCircle);
        break;
    }
    }
}

CircleHelper::offset_set CircleHelper::getOffsets(const int radius) {
    if (radius > static_cast<int>(offsetTable_.size())) {
        getInstance().generateOffsetTable(radius);
    }
    return offsetTable_[radius];
}

CircleHelper::offset_set CircleHelper::getOffsetsUpTo(const int radius) {
    offset_set offsets;
    for (auto r = 0; r <= radius; ++r) {
        auto radiusSet = getInstance().getOffsets(r);
        offsets.insert(radiusSet.begin(), radiusSet.end());
    }
    return offsets;
}

CircleHelper::offset_set CircleHelper::manhattanCircle(const int radius) const {
    offset_set circleOffsets;

    for (auto x = 0; x <= radius; ++x) {
        for (auto y = 0; y <= radius; ++y) {
            if (abs(x + y) == radius) {
                circleOffsets.emplace(-x, -y);
                circleOffsets.emplace(-x, +y);
                circleOffsets.emplace(+x, -y);
                circleOffsets.emplace(+x, +y);
            }
        }
    }
    return circleOffsets;
}

CircleHelper::offset_set CircleHelper::square(const int radius) const {
    offset_set circleOffsets;

    for (auto x = 0; x <= radius; x++) {
        for (auto y = 0; y <= radius; y++) {
            if (abs(x) == radius || abs(y) == radius) {
                circleOffsets.emplace(-x, -y);
                circleOffsets.emplace(-x, +y);
                circleOffsets.emplace(+x, -y);
                circleOffsets.emplace(+x, +y);
            }
        }
    }
    return circleOffsets;
}

CircleHelper::offset_set CircleHelper::midpointCircle(const int radius) const {
    auto filledCircle = filledMidpoint(radius);
    offset_set circleOffsets;

    for (auto i = 0; i < radius; i++) {
        std::set_difference(filledCircle.begin(), filledCircle.end(),
                            offsetTable_[i].begin(), offsetTable_[i].end(),
                            std::inserter(circleOffsets, circleOffsets.end()));
    }

    return circleOffsets;
}

CircleHelper::offset_set CircleHelper::getLine(const Position from, const Position to) {
    offset_set line;
    if (from.y == to.y) {
        for (auto x = from.x; x < to.x; x++) {
            line.emplace(x, from.y);
        }
    }
    return line;
}

CircleHelper::offset_set CircleHelper::filledMidpoint(const int radius) {
    offset_set circleOffsets;

    auto x = radius;
    auto y = 0;
    auto dx = 1, dy = 1;
    auto err = dx - (radius << 1);

    while (x >= y) {
        offset_set_vec lines;
        lines.push_back(getLine(Position(-x, -y), Position(+x, -y)));
        lines.push_back(getLine(Position(-x, +y), Position(+x, +y)));
        lines.push_back(getLine(Position(-y, -x), Position(+y, -x)));
        lines.push_back(getLine(Position(-y, +x), Position(+y, +x)));

        for (auto const& line : lines) {
            circleOffsets.insert(line.begin(), line.end());
        }

        if (err <= 0) {
            y++;
            err += dy;
            dy += 2;
        }

        if (err > 0) {
            x--;
            dx += 2;
            err += dx - (radius << 1);
        }
    }
    return circleOffsets;
}

float CircleHelper::geometricSeries(const int radius) const {
    return static_cast<float>(pow(0.5, radius + 1));
}

float CircleHelper::getFactor(const int radius) const {
    return (this->*factorFunction_)(radius);
}

CircleHelper::CircleHelper(offset_set (CircleHelper::* circleFunction)(const int) const,
                           float (CircleHelper::*factorFunction)(const int) const)
    : circleFunction_(circleFunction),
      factorFunction_(factorFunction) {
    generateOffsetTable();

}

