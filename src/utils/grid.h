//
// Created by stenmark on 05.03.2018.
//

#ifndef PROCEDURALFOREST_GRID_H
#define PROCEDURALFOREST_GRID_H


#include <array>

#include "Position.h"

namespace grid {
    /**
     * \brief template implementation for a 2D grid flattened to an array
     * \tparam Tp value_type
     * \tparam Width width of the grid
     * \tparam Height height of the grid
     */
    template<typename Tp, int Width, int Height>
    struct grid : std::array<Tp, Width * Height> {
        typedef Tp value_type;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef std::array<value_type, Width * Height> array;

        using array::array;

        // element access
        reference at(const int x, const int y) { return array::at(indexFromCoordinates(x, y)); }

        const_reference at(const int x, const int y) const { return array::at(indexFromCoordinates(x, y)); }

        reference at(const Position pos) { return array::at(indexFromPosition(pos)); }

        const_reference at(const Position pos) const { return array::at(indexFromPosition(pos)); }

        reference operator[](const Position pos) { return array::operator[](indexFromPosition(pos)); }

        const_reference operator[](const Position pos) const { return array::operator[](indexFromPosition(pos)); }


        /**
         * \brief creates a 2DPosition from an iterator
         * \param iterator 
         * \return Position in the Grid (-1 | -1) if invalid
         */
        Position positionFromIterator(typename array::const_iterator iterator) {
            if (iterator == this->array::end()) { return {-1, -1}; }
            const auto index = 
                static_cast<int>(iterator - this->array::begin());
            return positionFromIndex(index);
        }

    private:

        
        /**
         * \brief calculates index to the underlying array from a given position
         * \param pos 
         * \return array-index
         */
        static int indexFromPosition(const Position& pos) {
            if (pos.x >= Width || pos.y >= Height) {
                throw std::out_of_range("position out of range");
            }
            return pos.y * Width + pos.x;
        }

        static Position positionFromIndex(const int index) {
            return {index % Width, index / Height};
        }

        /**
         * \brief calculates index into flattened array from 2D position
         * \param x x-coordinate
         * \param y y-coordinate
         * \return array-index
         */
        static int indexFromCoordinates(const int x, const int y) {
            return indexFromPosition({x, y});           
        }

        


    };
}


#endif //PROCEDURALFOREST_GRID_H
