//
// Created by enrico on 12/04/18.
//

#ifndef PROCEDURALFOREST_FITNESSCALCULATOR_H
#define PROCEDURALFOREST_FITNESSCALCULATOR_H


#include "../ResourceContainer.h"

/**
 * \brief Calculates how often demand fits into supply
 * \param supply 
 * \param demand 
 * \return fitness in generations
 */
inline int CalculateFitness(const ResourceSupply& supply, const ResourceDemand demand) {
    auto generations = 0;
    if (demand == ResourceDemand()) {return 0;}
    auto newDemand = demand;
    while (newDemand < supply) {
        newDemand *= static_cast<float>(++generations);
    }
    return generations;
}
#endif //PROCEDURALFOREST_FITNESSCALCULATOR_H
