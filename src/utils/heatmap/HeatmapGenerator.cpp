//
// Created by enrico on 05/04/18.
//

#include "HeatmapGenerator.h"
#include <filesystem>
#include <iomanip>
#include <sstream>

HeatmapGenerator::HeatmapGenerator(grid_ptr resourceGrid)
    : resourceGrid_(resourceGrid) {

    hm_ = heatmap_new(grid_width, grid_height);
    hmStamp_ = heatmap_stamp_gen(0);
}

HeatmapGenerator::~HeatmapGenerator() {
    heatmap_free(hm_);
    heatmap_stamp_free(hmStamp_);
}

float HeatmapGenerator::dataForMode(const grid_type::const_iterator it, const DrawMode mode) const {
    const auto light = 0.f; //it->getLight().GetValue();
    const auto water = 0.f; //it->getWater().GetValue();
    const auto carbon = it->getCarbon().getValue();
    const auto nitrogen = it->getNitrogen().getValue();
    const auto phosphor = it->getPhosphor().getValue();

    switch (mode) {
    case DM_LIGHT: return light;
    case DM_WATER: return water;
    case DM_CARBON: return carbon;
    case DM_NITROGEN: return nitrogen;
    case DM_PHOSPHOR: return phosphor;
    case DM_SUM: return (carbon + nitrogen + phosphor);
    case DM_MEAN: return (carbon + nitrogen + phosphor) / 3.f;
    case DM_MEDIAN: return std::max(std::min(carbon, nitrogen), std::min(std::max(carbon, nitrogen), phosphor));
    }
    return 0.f;
}

void HeatmapGenerator::createHeatmap(const DrawMode mode) {
    heatmap_free(hm_);
    hm_ = heatmap_new(grid_width, grid_height);
    for (auto it = resourceGrid_->begin(); it != resourceGrid_->end(); ++it) {
        const auto position = resourceGrid_->positionFromIterator(it);
        heatmap_add_weighted_point_with_stamp(hm_,
                                              position.x,
                                              position.y,
                                              dataForMode(it, mode),
                                              hmStamp_
        );
    }
}

void HeatmapGenerator::draw(const int iteration) {
    draw(iteration, DM_CARBON);
    draw(iteration, DM_NITROGEN);
    draw(iteration, DM_PHOSPHOR);
    draw(iteration, DM_SUM);
    draw(iteration, DM_MEAN);
    draw(iteration, DM_MEDIAN);
}

void HeatmapGenerator::draw(const int iteration, const DrawMode mode) {
    createHeatmap(mode);
    drawHeatmap(iteration, mode);
}

void HeatmapGenerator::drawHeatmap(const int iteration, const DrawMode mode) const {
    std::vector<unsigned char> image(grid_width * grid_height * 4);

    heatmap_render_default_to(hm_, &image[0]);
    const auto path = std::experimental::filesystem::current_path().string() + "\\heatmaps\\" + modeToString(mode) + "\\";

    std::ostringstream iterationStr;
    iterationStr << std::setfill('0') << std::setw(3) << iteration;

    const auto filename = "it_" + iterationStr.str() + ".png";

    const auto errCode = lodepng::encode(path + filename, image, grid_width, grid_height);

    LOG_INFO(std::string(("iteration:"+ iterationStr.str() + " - drawing " + modeToString(mode) + " - " +
        lodepng_error_text(errCode))));

}

std::string HeatmapGenerator::modeToString(const DrawMode mode) {
    switch (mode) {

    case DM_LIGHT: return "light";
    case DM_WATER: return "water";
    case DM_CARBON: return "carbon";
    case DM_NITROGEN: return "nitrogen";
    case DM_PHOSPHOR: return "phosphor";
    case DM_SUM: return "sum";
    case DM_MEAN: return "mean";
    case DM_MEDIAN: return "median";
    }
    return "no mode";
}
