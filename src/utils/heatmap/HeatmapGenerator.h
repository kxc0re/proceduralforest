//
// Created by enrico on 05/04/18.
//

#ifndef PROCEDURALFOREST_HEATMAPGENERATOR_H
#define PROCEDURALFOREST_HEATMAPGENERATOR_H

#include "../typedefs.h"
#include "../lodepng/lodepng.h"
#include "../grid.h"
#include "../../ResourceContainer.h"

#include "heatmap.h"

class ResourceGrid;

enum DrawMode {
    DM_LIGHT,
    DM_WATER,
    DM_CARBON,
    DM_NITROGEN,
    DM_PHOSPHOR,
    DM_SUM,
    DM_MEAN,
    DM_MEDIAN,
    FIRST = DM_LIGHT,
    LAST = DM_MEDIAN
};


class HeatmapGenerator {
    typedef grid::grid<ResourceSupply, grid_width, grid_height> grid_type;
    typedef grid::grid<ResourceSupply, grid_width, grid_height>* grid_ptr;
    
    heatmap_t* hm_;
    heatmap_stamp_t* hmStamp_;
    grid_ptr resourceGrid_ = nullptr;


public:
    HeatmapGenerator() = delete;
    HeatmapGenerator(const HeatmapGenerator&) = delete;
    HeatmapGenerator(HeatmapGenerator&&) = delete;
    HeatmapGenerator& operator=(HeatmapGenerator&&) = delete;
    HeatmapGenerator& operator=(const HeatmapGenerator&) = delete;

    explicit HeatmapGenerator(grid_ptr resourceGrid);

    ~HeatmapGenerator();

    float dataForMode(grid_type::const_iterator it, DrawMode mode) const;

    void createHeatmap(DrawMode mode);

    void draw(int iteration);

    void draw(int iteration, DrawMode mode);

    void drawHeatmap(int iteration, DrawMode mode) const;

    static std::string modeToString(DrawMode mode);

};



#endif //PROCEDURALFOREST_HEATMAPGENERATOR_H
