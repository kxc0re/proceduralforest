//
// Created by enrico on 19/04/18.
//

#ifndef PROCEDURALFOREST_TREEMAPGENERATOR_H
#define PROCEDURALFOREST_TREEMAPGENERATOR_H

#include <vector>
#include <map>
#include <memory>

#include "../../modifier/ResourceModifier.h"

class ResourceModifierRegistry;


struct Color {
    unsigned char r, g, b, a;

    Color(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha = 255);
    Color();
};

class TreemapGenerator {

    inline static Color black_ = {0,0,0,255};


    std::map<ResourceModifierType, Color> colorMap_ = {
        {AVG_TREE,     {255, 255,   0}}, 
        {TREE_CARBON,  {255,   0,   0}},
        {TREE_NITRO,   {  0, 255,   0}},
        {TREE_PHOS,    {  0,   0, 255}}
        };


    std::unique_ptr<std::vector<unsigned char>> image_;

    ResourceModifierRegistry* modifierRegistry_ = nullptr;


  public:
    explicit TreemapGenerator(ResourceModifierRegistry* registry);

    Color& typeToColor(ResourceModifierType type);

    void createTreemap();

    void draw(int iteration);

};


#endif //PROCEDURALFOREST_TREEMAPGENERATOR_H
