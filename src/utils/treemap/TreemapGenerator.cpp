//
// Created by enrico on 19/04/18.
//

#include <string>

#include "TreemapGenerator.h"
#include "../../modifier/ResourceModifierRegistry.h"
#include "../heatmap/HeatmapGenerator.h"
#include <filesystem>
#include <iomanip>
#include <sstream>

Color::Color(const unsigned char red, const unsigned char green, const unsigned char blue, const unsigned char alpha)
    : r(red), g(green), b(blue), a(alpha) {}

Color::Color()
    : r(0), g(0), b(0), a(0) {}

TreemapGenerator::TreemapGenerator(ResourceModifierRegistry* registry)
    : image_(std::make_unique<std::vector<unsigned char>>()), modifierRegistry_(registry) {}

Color& TreemapGenerator::typeToColor(const ResourceModifierType type) {
    return colorMap_[type];
}

void TreemapGenerator::createTreemap() {
    image_->clear();
    auto colors = std::make_unique<grid::grid<Color, grid_width, grid_height>>();
    colors->fill(Color());

    for (auto && [position, modifier] : *modifierRegistry_) {
        colors->at(position) = typeToColor(modifier->getType());
    }


    for (auto&& [r, g, b, a] : *colors) {
        image_->push_back(r);
        image_->push_back(g);
        image_->push_back(b);
        image_->push_back(a);
    }

}

void TreemapGenerator::draw(const int iteration) {
    createTreemap();

    const auto path = std::experimental::filesystem::current_path().string() + "\\treemaps\\";

    std::ostringstream iterationStr;
    iterationStr << std::setfill('0') << std::setw(3) << iteration;

    const auto filename = "it_" + iterationStr.str() + ".png";

    const auto errCode = lodepng::encode(path + filename, *image_, grid_width, grid_height);

    LOG_INFO(std::string(
        ("iteration:" + iterationStr.str() + " - drawing treemap - " + lodepng_error_text(errCode))))

    
}
