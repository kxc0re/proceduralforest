//
// Created by stenmark on 06.03.2018.
//

#ifndef PROCEDURALFOREST_POSITION_H
#define PROCEDURALFOREST_POSITION_H

#include "typedefs.h"


/**
 * \brief Implementation of 2D Vector in cartesian coordinates
 */
struct Position  {
    int x;
    int y;

    Position(const int x = 0, const int y = 0) : x(x), y(y) {}

    Position& operator+=(const Position& rhs) {
        x += rhs.x;
        y += rhs.y;
        return *this;
    }

    Position& operator-=(const Position& rhs) {
        x -= rhs.x;
        y -= rhs.y;
        return *this;
    }

    bool isNegative() const;

    bool isOutOfBounds() const;

    bool isInvalid() const;

    friend bool operator==(const Position& lhs, const Position& rhs) {
        return lhs.x == rhs.x && lhs.y == rhs.y;
    }

    friend bool operator<(const Position& lhs, const Position& rhs) {
        return lhs.y < rhs.y || (lhs.y == rhs.y && lhs.x < rhs.x);
    }

    friend std::ostream& operator<<(std::ostream& output, const Position& rhs) {
        output << "(" << rhs.x << ", " << rhs.y << ")";
        return  output;
    }

};

inline bool operator!=(const Position& lhs, const Position& rhs) {
    return !operator==(lhs, rhs);
}

inline bool operator>=(const Position& lhs, const Position& rhs) {
    return !operator<(lhs, rhs);
}
inline bool operator>(const Position& lhs, const Position& rhs) {
    return operator<(rhs, lhs);
}
inline bool operator<=(const Position& lhs, const Position& rhs) {
    return !operator<(rhs, lhs);
}



inline bool Position::isNegative() const {
    return x < 0 || y < 0;
}

inline bool Position::isOutOfBounds() const {
    return  x >= grid_width || y >= grid_height;
}

inline bool Position::isInvalid() const {
    return isNegative() || isOutOfBounds();
}

inline Position operator+(Position lhs, const Position& rhs) {
    return lhs += rhs;
}

inline Position operator-(Position lhs, const Position& rhs) {
    return lhs -= rhs;
}


#endif //PROCEDURALFOREST_POSITION_H
