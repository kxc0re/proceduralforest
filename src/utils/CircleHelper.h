//
// Created by k3ksc on 27/02/2018.
//

#ifndef PROCEDURALFOREST_CIRCLEHELPER_H
#define PROCEDURALFOREST_CIRCLEHELPER_H


#include <vector>
#include <set>

#include "Position.h"


#define MAX_RADIUS 100

class ResourceGrid;

enum CircleFunction {
    MANHATTAN,
    MIDPOINT,
    SQUARE
};

class CircleHelper {
    typedef std::set<Position> offset_set;
    typedef std::vector<offset_set> offset_set_vec;

    offset_set_vec offsetTable_;

    offset_set (CircleHelper::*circleFunction_)(const int) const;
    float (CircleHelper::*factorFunction_)(const int) const;

    CircleHelper(offset_set (CircleHelper::*circleFunction)(const int) const,
                 float (CircleHelper::*factorFunction)(const int) const);

    void generateOffsetTable(int radius = MAX_RADIUS, bool overwrite = false);

    /**
     * Genereates a filled MidpointCircle
     * @param radius
     * @return offset_set with all offsets to the center
     */
    static offset_set filledMidpoint(int radius);

    /**
     * Generates a Line
     * @param from position from where the line should be drawn
     * @param to position to where the line should be drawn
     * @return offset_set with all offsets in the line
     */
    static offset_set getLine(Position from, Position to);

    /**
     * Generates a Ring with Manhattan-Distance/TaxiCab-Geometry
     * @param radius of the ring (radius + 1 = a)
     * @return offset_set with all offsets to the center
     */
    offset_set manhattanCircle(int radius) const;

    /**
     * Generates a Ring of a Square
     * @param radius of the square (2*radius + 1 = a)
     * @return offset_set with all offsets to the center
     */
    offset_set square(int radius) const;

    /**
     * Generates a Ring of a MidpointCircle
     * @param radius
     * @return offset_set with all offsets to the center
     */
    offset_set midpointCircle(int radius) const;

    /**
     * \brief generates a factor based on the geometric series
     * \param radius 
     * \return factor at radius
     */
    float geometricSeries(int radius) const;

    void setCircleFunction(offset_set (CircleHelper::*circleFunction)(const int) const);
    void setFactorFunction(float (CircleHelper::*factorFunction)(const int) const);

public:

    static CircleHelper& getInstance(
        offset_set (CircleHelper::*circleFunction)(const int) const = &CircleHelper::square,
        float (CircleHelper::*factorFunction)(const int) const = &CircleHelper::geometricSeries);

    CircleHelper(CircleHelper const&) = delete;

    void operator=(CircleHelper const&) = delete;

    void setCircleFunction(CircleFunction fun) const;

    offset_set getOffsets(int radius);
    static offset_set getOffsetsUpTo(int radius);

    float getFactor(int radius) const;
};


#endif //PROCEDURALFOREST_CIRCLEHELPER_H
