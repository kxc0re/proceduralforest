#include <cstdlib>
#include "World.h"
#include <filesystem>
#include <iostream>

//#pragma comment(linker, "/STACK:40000000")

            
int main() {
    World world;
    const auto iterations = 100;

    for (auto i = 1; i <= iterations; ++i)
        world.tick();

    system("pause");
    return EXIT_SUCCESS;
}



