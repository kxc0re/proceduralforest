//
// Created by enrico on 05/04/18.
//

#ifndef PROCEDURALFOREST_MODIFIERREGISTRY_H
#define PROCEDURALFOREST_MODIFIERREGISTRY_H


#include <map>
#include <memory>

#include "../utils/treemap/TreemapGenerator.h"
#include "../utils/Position.h"
#include "../utils/typedefs.h"
#include "../utils/grid.h"

#include "ResourceModifier.h"
#include "ResourceModifierFactory.h"


class ResourceModifierRegistry {
    typedef std::map<Position, std::unique_ptr<ResourceModifier>> mod_map;
    typedef std::tuple<Position, int, ResourceModifierType> exp_candidate;
    typedef std::multimap<int, Position, std::greater<>> candidate_mm;
    typedef std::map<Position, std::pair<int, ResourceModifierType>> exp_map;
    typedef std::vector<std::pair<Position, ResourceModifierType>> type_vec;
    typedef grid::grid<ResourceSupply, grid_width, grid_height>* grid_ptr;
    typedef std::vector<Position> pos_vec;


    /**
     * \brief actual registry composed of the modifiers
     */
    mod_map resourceModifiers_;
    /**
     * \brief global expansionCandidates
     */
    exp_map expansionCandidates_;
    /**
     * \brief Pointer to the ResourceGrid
     */
    grid_ptr resourceGrid_;
    /**
     * \brief Modifiers marked for deletion
     */
    pos_vec toBeDeleted_;
    ResourceModifierFactory* resourceModifierFactory_;

    /**
     * \brief Consume-Step
     * Applies the demands for every registered Modifiers, marks for deletion when demands cannot be met
     */
    void consume();

    /**
     * \brief Produce-Step
     * Adds Supply to the surrounding Cells for every modifier
     */
    void produce();
    /**
     * \brief Expand-Step
     * Creates new Modifiers based on the best fit
     */
    void expand();

    /**
     * \brief Grow-Step
     * every modifier grows, each update until maturity
     */
    void grow();

    /**
     * \brief deletes marked modifiers
     */
    void deleteModifier();
    /**
     * \brief finds the best fittin positions for expansion per modifier
     */
    void prepareExpand();

public:
    explicit ResourceModifierRegistry(grid_ptr resGrid, ResourceModifierFactory* factory);

    
    mod_map::iterator begin();
    mod_map::const_iterator begin() const;
    mod_map::iterator end();
    mod_map::const_iterator end() const;

 

    /**
     * \brief UpdateCycle
     * calls consume() -> produce() -> expand() -> grow() for every registered Modifier
     *
     */
    void update();

    /**
     * \brief Registers Modifier in the Registry, overwrites if position already occupied
     * \param position where in the grid
     * \param modifier the modifier to be registered
     */
    void registerModifier(Position position, std::unique_ptr<ResourceModifier> modifier);

    /**
     * \brief Marks Modifier for deletion
     * \param position postion, on which the modifier should be deleted
     */
    void deregisterModifier(Position position);


};


#endif //PROCEDURALFOREST_MODIFIERREGISTRY_H
