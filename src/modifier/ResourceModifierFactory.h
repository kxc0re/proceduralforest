//
// Created by k3ksc on 18/04/2018.
//

#ifndef PROCEDURALFOREST_RESOURCEMODIFIERFACTORY_H
#define PROCEDURALFOREST_RESOURCEMODIFIERFACTORY_H

#include <functional>

#include "../ResourceContainer.h"
#include "../utils/RandomGenerator.h"
#include "../utils/Position.h"

#include "ResourceModifier.h"


class ResourceModifierFactory {
    typedef std::unique_ptr<ResourceModifier> mod_ptr;
    typedef std::vector<std::pair<Position, mod_ptr>> mod_vec;
    typedef std::vector<ResourceModifierState> state_vec;

    enum ResourceClass {
        LO = 20,
        MD = 50,
        HI = 80
    };

    float growthFactor_ = 1.1f;


    mod_ptr createTree(ResourceModifierType type, state_vec modifierStates, unsigned int level = 0) const;

    mod_ptr createTree(ResourceModifierType type, ResourceDemand baseDemand, ResourceSupply baseSupply,
                       int baseSupplyRadius,
                       float baseChance, int baseRadius, int baseCount,
                       unsigned int maxLevel, float growthFactor,
                       unsigned int level = 0) const;

public:
    mod_ptr createPerfectConsumer(unsigned int level = 0) const;

    mod_ptr createWorstConsumer(unsigned int level = 0) const;

    mod_ptr createAverageConsumer(unsigned int level = 0) const;

    mod_ptr createAverageTree(unsigned int level = 0) const;

    mod_ptr createCarbonTree(unsigned int level = 0) const;

    mod_ptr createNitrogenTree(unsigned int level = 0) const;

    mod_ptr createPhosphorTree(unsigned int level = 0) const;

    mod_ptr createFromType(ResourceModifierType type) const;

};


#endif //PROCEDURALFOREST_RESOURCEMODIFIERFACTORY_H
