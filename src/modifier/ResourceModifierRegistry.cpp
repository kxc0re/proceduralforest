//
// Created by k3ksc on 07/04/2018.
//

#include "ResourceModifierRegistry.h"
#include "../utils/CircleHelper.h"
#include <functional>
#include "../utils/FitnessCalculator.h"
#include <algorithm>
#include "ResourceModifierFactory.h"
#include <map>
#include <iterator>

ResourceModifierRegistry::ResourceModifierRegistry(grid_ptr resGrid, ResourceModifierFactory* factory)
    : resourceGrid_(resGrid),
      resourceModifierFactory_(factory) {
    resourceModifiers_.clear();
}

ResourceModifierRegistry::mod_map::iterator ResourceModifierRegistry::begin() {
    return resourceModifiers_.begin();
}

ResourceModifierRegistry::mod_map::const_iterator ResourceModifierRegistry::begin() const {
    return resourceModifiers_.begin();
}

ResourceModifierRegistry::mod_map::iterator ResourceModifierRegistry::end() {
    return resourceModifiers_.end();
}

ResourceModifierRegistry::mod_map::const_iterator ResourceModifierRegistry::end() const {
    return resourceModifiers_.end();
}

void ResourceModifierRegistry::consume() {
    LOG_DEBUG("consume")
    for (const auto& [position, modifier] : *this) {

        const auto demand = modifier->getCurrentDemand();
        const auto supply = resourceGrid_->at(position);

        if (supply.isNegative() || supply.isInfinite()) { throw std::exception("supply invalid"); }

        if (demand > supply || demand.isInvalid()) {
            deregisterModifier(position);
        } else { resourceGrid_->at(position) -= demand; }
    }
    deleteModifier();
}

void ResourceModifierRegistry::produce() {
    LOG_DEBUG("produce")
    for (const auto& [position, modifier] : resourceModifiers_) {


        const auto supply = modifier->getCurrentSupply();
        const auto radius = modifier->getSupplyRadius();

        if (radius > 100) {
            LOG_ERROR("invalid modifier @" << position << " modifier: " << *modifier);
            continue;
            //throw std::exception("invalid modifier");
        }

        if (supply.isInfinite()) { throw std::exception("supply is infinite"); }
        if (supply.isNegative()) { throw std::exception("supply is negative"); }


        for (auto i = 0; i <= radius; ++i) {
            auto offsets = CircleHelper::getInstance().getOffsets(i);
            const auto factor = CircleHelper::getInstance().getFactor(i);

            for (const auto& offset : offsets) {
                const auto prodPosition = position + offset;
                if (prodPosition.isInvalid()) { continue; }

                resourceGrid_->at(prodPosition) += supply * (factor * offsets.size());
            }
        }
    }
}

void ResourceModifierRegistry::prepareExpand() {
    for (const auto& [position, modifier] : resourceModifiers_) {

        auto offsets = CircleHelper::getOffsetsUpTo(modifier->getSproutingRadius());

        std::map<Position, int> bestPositions;

        for (const auto& offset : offsets) {
            auto expPosition = position + offset;

            // skip if invalid
            if (expPosition.isInvalid()) { continue; }

            //skip if sproutPosition is occupied already
            //if (resourceModifiers_.find(sproutPosition) != resourceModifiers_.end()) { continue; }


            const auto supply = resourceGrid_->at(expPosition);
            const auto demand = modifier->getBaseDemand();

            const auto fitness = CalculateFitness(supply, demand);


            if (const auto it = bestPositions.find(expPosition); it != bestPositions.end() && it->second < fitness) {
                //overwrite fitness if fitting better
                it->second = fitness;
            } else { bestPositions.emplace(expPosition, fitness); } // add new position

        }

        // sort bestPositions by fitness
        std::multimap<int, Position, std::greater<>> bestFitting;
        for (const auto& [pos, fitness] : bestPositions) {
            bestFitting.emplace(fitness, pos);
        }

        // insert the sproutingCount best fitting candidates into the global candidates
        for (auto bestFit = bestFitting.begin(); bestFit != std::next(bestFitting.begin(),
                                                                      modifier->getSproutingCount()); ++bestFit) {
            const auto& [fitness, expPosition] = *bestFit;
            auto candidate = expansionCandidates_.find(expPosition);

            if (candidate == expansionCandidates_.end()) {
                expansionCandidates_.emplace(expPosition, std::make_pair(fitness, modifier->getType()));
            } else if (const auto& [oldFitness, oldType] = candidate->second; 
                        fitness > oldFitness || (fitness == oldFitness && isMoreAggressive(modifier->getType(), oldType))) {
                candidate->second = {fitness, modifier->getType()};
            }
        }

    }
}

void ResourceModifierRegistry::expand() {
    prepareExpand();

    for (const auto& [position, pair] : expansionCandidates_) {
        const auto type = pair.second;

        registerModifier(position, resourceModifierFactory_->createFromType(type));
    }

    LOG_DEBUG("expand " << expansionCandidates_.size() << " candidates modifier count now is " << resourceModifiers_.
        size());
    expansionCandidates_.clear();
}

void ResourceModifierRegistry::grow() {
    LOG_DEBUG("grow")
    for (const auto& [position, modifier] : resourceModifiers_) {
        modifier->grow();
    }
}

void ResourceModifierRegistry::deleteModifier() {
    LOG_DEBUG("deleting " << toBeDeleted_.size() << " of " << resourceModifiers_.size() << " modifiers")
    for (const auto& position : toBeDeleted_) {
        resourceModifiers_.erase(position);
    }
    toBeDeleted_.clear();
}


void ResourceModifierRegistry::update() {
    consume();
    produce();
    expand();
    grow();

}

void ResourceModifierRegistry::registerModifier(const Position position, std::unique_ptr<ResourceModifier> modifier) {
    resourceModifiers_.insert_or_assign(position, std::move(modifier));
}

void ResourceModifierRegistry::deregisterModifier(const Position position) {
    toBeDeleted_.push_back(position);
}
