//
// Created by enrico on 16/04/18.
//

#ifndef PROCEDURALFOREST_RESOURCEMODIFIERSTATE_H
#define PROCEDURALFOREST_RESOURCEMODIFIERSTATE_H


#include "../ResourceContainer.h"

struct ResourceModifierState {

    ResourceDemand demand;

    ResourceSupply supply;
    int supplyRadius;

    float sproutingChance;
    int sproutingRadius;
    int sproutingCount;

    ResourceModifierState(ResourceDemand demand, ResourceSupply supply, int supplyRadius,
                          float sproutingChance, int sproutingRadius, int sproutingCount);

};


#endif //PROCEDURALFOREST_RESOURCEMODIFIERSTATE_H
