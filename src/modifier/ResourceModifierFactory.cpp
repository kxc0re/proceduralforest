//
// Created by k3ksc on 18/04/2018.
//

#include "ResourceModifierFactory.h"

/**
 * \brief Creates new Modifier
 * \param type kind of tree
 * \param modifierStates the modifierState-Table
 * \param level current level
 * \return new Modifier
 */
ResourceModifierFactory::mod_ptr ResourceModifierFactory::createTree(const ResourceModifierType type,
                                                                     ResourceModifierFactory::state_vec modifierStates,
                                                                     const unsigned int level) const {
    return std::make_unique<ResourceModifier>(modifierStates, level, type);
}

/**
 * \brief Primarily creates the modifierState-Table via growthFactor
 * \param type Which kind of tree
 * \param baseDemand 
 * \param baseSupply 
 * \param baseSupplyRadius radius used in the produce() step
 * \param baseChance sproutingChance used in expand()
 * \param baseRadius radius used in expand()
 * \param baseCount count of new sprouts per round
 * \param maxLevel 
 * \param growthFactor factor the base values should rise per level
 * \param level start level
 * \return new Modifier
 */
ResourceModifierFactory::mod_ptr ResourceModifierFactory::createTree(const ResourceModifierType type,
                                                                     const ResourceDemand baseDemand,
                                                                     const ResourceSupply baseSupply,
                                                                     const int baseSupplyRadius,
                                                                     const float baseChance, const int baseRadius,
                                                                     const int baseCount,
                                                                     const unsigned int maxLevel,
                                                                     const float growthFactor,
                                                                     const unsigned int level) const {
    state_vec modifierStates;

    for (unsigned int i = 0; i < maxLevel; ++i) {
        const auto factor = static_cast<float>(pow(growthFactor, i));

        auto demand = baseDemand * factor;
        auto supply = baseSupply * factor;
        auto supplyRadius = static_cast<int>(baseSupplyRadius * factor);


        auto sproutingChance = baseChance * factor;
        auto sproutingRadius = static_cast<int>(baseRadius * factor);
        auto sproutingCount = static_cast<int>(baseCount * factor);


        modifierStates.emplace_back(demand, supply, supplyRadius, sproutingChance, sproutingRadius, sproutingCount);
    }

    return createTree(type, modifierStates, level);

}

ResourceModifierFactory::mod_ptr ResourceModifierFactory::createPerfectConsumer(const unsigned int level) const {
    const ResourceDemand lowDemand = {0.f, 0.f, LO, LO, LO};
    const ResourceSupply noProduce = {};

    return createTree(PERF_CONSUMER, lowDemand, noProduce, 0, HI / 100.f, HI / 10, 4, 3, growthFactor_, level);
}

ResourceModifierFactory::mod_ptr ResourceModifierFactory::createWorstConsumer(const unsigned int level) const {
    const ResourceDemand highDemand = {0.f, 0.f, HI, HI, HI};
    const ResourceSupply noProduce = {};

    return createTree(WORST_CONSUMER, highDemand, noProduce, 0, LO / 100.f, HI / 10, 4, 3, growthFactor_, level);
}

ResourceModifierFactory::mod_ptr ResourceModifierFactory::createAverageConsumer(const unsigned int level) const {
    const ResourceDemand medDemand = {0.f, 0.f, MD, MD, MD};
    const ResourceSupply noProduce = {};

    return createTree(AVG_CONSUMER, medDemand, noProduce, 0, MD / 100.f, MD / 10, 4, 3, growthFactor_, level);
}

ResourceModifierFactory::mod_ptr ResourceModifierFactory::createAverageTree(const unsigned int level) const {
    const ResourceDemand medDemand = {0.f, 0.f, MD, MD, MD};
    const ResourceSupply medSupply = {0.f, 0.f, MD, MD, MD};

    return createTree(AVG_TREE, medDemand, medSupply, MD / 10, MD / 100.f, MD / 10, 4, 5, growthFactor_, level);
}

ResourceModifierFactory::mod_ptr ResourceModifierFactory::createCarbonTree(const unsigned int level) const {
    const ResourceDemand demand = {0.f, 0.f, HI, MD, LO};
    const ResourceSupply supply = {0.f, 0.f, LO, MD, HI};

    return createTree(TREE_CARBON, demand, supply, 5, 0.3f, 5, 4, 5, 1.1f, level);
}

ResourceModifierFactory::mod_ptr ResourceModifierFactory::createNitrogenTree(const unsigned int level) const {
    const ResourceDemand demand = {0.f, 0.f, LO, HI, LO};
    const ResourceSupply supply = {0.f, 0.f, HI, LO, HI};

    return createTree(TREE_NITRO, demand, supply, 4, 0.2f, 4, 4, 8, 1.05f, level);

}

ResourceModifierFactory::mod_ptr ResourceModifierFactory::createPhosphorTree(const unsigned int level) const {
    const ResourceDemand demand = {0.f, 0.f, MD, LO, HI};
    const ResourceSupply supply = {0.f, 0.f, MD, MD, LO};

    return createTree(TREE_PHOS,
                      demand, supply,
                      8, 0.1f, 5, 6, 3, 1.2f,
                      level);
}

/**
 * \brief Create new Modifier from type
 * \param type 
 * \return new Modifier
 */
ResourceModifierFactory::mod_ptr ResourceModifierFactory::createFromType(const ResourceModifierType type) const {
    switch (type) {
    case TREE_CARBON: return createCarbonTree(0);
    case TREE_NITRO: return createNitrogenTree(0);
    case TREE_PHOS: return createPhosphorTree(0);
    case AVG_TREE:
    default: return createAverageTree(0);
    }
}
