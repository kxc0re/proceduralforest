//
// Created by enrico on 16/04/18.
//

#include "ResourceModifierState.h"

ResourceModifierState::ResourceModifierState(ResourceDemand demand, ResourceSupply supply, int supplyRadius,
                                             float sproutingChance, int sproutingRadius, int sproutingCount)
    : demand(demand), supply(supply), supplyRadius(supplyRadius),
      sproutingChance(sproutingChance), sproutingRadius(sproutingRadius), sproutingCount(sproutingCount) {}
