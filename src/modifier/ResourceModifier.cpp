//
// Created by kxc0re on 02.03.2018.
//

#include "ResourceModifier.h"
#include <utility>

ResourceModifier::ResourceModifier(state_vec modifierStates, const unsigned int level,
                                   const ResourceModifierType type)
    : resourceModifierStates_(std::move(modifierStates)),
      type_(type) {
    if(level < resourceModifierStates_.size()) {
        currentState_ = resourceModifierStates_.begin() + level;
    }
    else throw std::exception("level too big");
}

ResourceDemand ResourceModifier::getCurrentDemand() const { return currentState_->demand; }

ResourceDemand ResourceModifier::getBaseDemand() const { return resourceModifierStates_.begin()->demand; }

ResourceSupply ResourceModifier::getCurrentSupply() const { return currentState_->supply; }

int ResourceModifier::getSupplyRadius() const { return currentState_->supplyRadius; }

float ResourceModifier::getSproutingChance() const { return currentState_->sproutingChance; }

int ResourceModifier::getSproutingRadius() const { return currentState_->sproutingRadius; }

int ResourceModifier::getSproutingCount() const { return currentState_->sproutingCount;}

ResourceModifierType ResourceModifier::getType() const { return type_; }

void ResourceModifier::grow() {
    if (currentState_ + 1 != resourceModifierStates_.end()) { ++currentState_; }
}

bool ResourceModifier::isInvalid() const {
    return getCurrentDemand().isInvalid() || getCurrentSupply().isInvalid() 
    || getSupplyRadius() < 0 || getSupplyRadius() > 100
    || getSproutingRadius() < 0 || getSproutingRadius() > 100;
}

const char* typeToString(ResourceModifierType type) {
    switch (type) {
    case AVG_TREE: return "AVG_TREE";
    case PERF_CONSUMER: return "PERF_CONSUMER";
    case AVG_CONSUMER: return "AVG_CONSUMER";
    case WORST_CONSUMER: return "WORST_CONSUMER";
    case TREE_CARBON: return "TREE_CARBON";
    case TREE_NITRO: return "TREE_NITRO";
    case TREE_PHOS: return "TREE_PHOS";
    default:return "" ;
    }
}

int getAggroValue(const ResourceModifierType lhs) {
    static std::map<ResourceModifierType, int> aggroValues = {
        {TREE_CARBON, 5},
        {TREE_NITRO, 3},
        {TREE_PHOS, 4},
        {AVG_TREE, 1}
    };

    if (const auto it = aggroValues.find(lhs); it != aggroValues.end()) {
        return it->second;
    }
    return 0;
}

bool isMoreAggressive(const ResourceModifierType lhs, const ResourceModifierType rhs) {
    return getAggroValue(lhs) > getAggroValue(rhs);
}

std::ostream& operator<<(std::ostream& output, ResourceModifier& rhs) {
    output << "{["
        << "Type: " << typeToString(rhs.type_) << ", "
        << "Demand: " << rhs.getCurrentDemand() << ", "
        << "Supply: " << rhs.getCurrentSupply() << ", "
        << "Level: " << rhs.currentState_ - rhs.resourceModifierStates_.begin() << "/" << rhs.resourceModifierStates_.size()
        << "]}";

    return output;
}
