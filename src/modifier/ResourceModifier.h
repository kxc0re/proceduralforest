//
// Created by kxc0re on 02.03.2018.
//

#ifndef PROCEDURALFOREST_RESOURCEMODIFIER_H
#define PROCEDURALFOREST_RESOURCEMODIFIER_H


#include <map>
#include <random>

#include "ResourceModifierState.h"


enum ResourceModifierType {
    AVG_TREE = 0,
    PERF_CONSUMER,
    AVG_CONSUMER,
    WORST_CONSUMER,
    TREE_CARBON,
    TREE_NITRO,
    TREE_PHOS
};


class ResourceModifier {
    typedef std::vector<ResourceModifierState> state_vec;

    state_vec resourceModifierStates_;
    state_vec::iterator currentState_;

    ResourceModifierType type_;


public:
    ResourceModifier(state_vec modifierStates, unsigned int level, ResourceModifierType type);

    ResourceDemand getCurrentDemand() const;
    ResourceDemand getBaseDemand() const;

    ResourceSupply getCurrentSupply() const;

    int getSupplyRadius() const;

    float getSproutingChance() const;

    int getSproutingRadius() const;

    int getSproutingCount() const;

    ResourceModifierType getType() const;

    void grow();

    friend std::ostream& operator<<(std::ostream& output, ResourceModifier& rhs);
    bool isInvalid() const;

};

const char* typeToString(ResourceModifierType type);

inline int getAggroValue(const ResourceModifierType lhs);

bool isMoreAggressive(ResourceModifierType lhs, ResourceModifierType rhs);

#endif //PROCEDURALFOREST_RESOURCEMODIFIER_H
