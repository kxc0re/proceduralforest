  var toggled = 0;
  var ctx = null;
  var toggleStyle = function() {
    if (toggled == 0) {
      ctx.fillStyle = "darkred";
      toggled++;
    } else if (toggled == 1) {
      ctx.fillStyle = "red";
      toggled++;
    } else if (toggled == 2) {
      ctx.fillStyle = "orange";
      toggled++;
    } else if (toggled == 3) {
      ctx.fillStyle = "white";
      toggled = 0;
    }
  };
  var DrawPixel = function(x, y) {
    ctx.beginPath();
    ctx.rect(150 + x * 10, 150 + y * 10, 10, 10);
    ctx.fill();
    ctx.stroke();
  };
  /*
   * Bresenham Circle (Midpoint Algorithm)
   * http://en.wikipedia.org/wiki/Midpoint_circle_algorithm
   */
  var DrawCircle = function(radius) {
    var x = radius;
    var y = 0;
    var dx = 1;
    var dy = 1;
    var err = dx - (radius << 1);
    while (x >= y) {
      DrawPixel(-x, -y);
      DrawPixel(+x, -y);
      DrawPixel(-x, +y);
      DrawPixel(+x, +y);
      DrawPixel(-y, -x);
      DrawPixel(+y, -x);
      DrawPixel(+y, +x);
      DrawPixel(-y, +x);
      if (err <= 0) {
        y++;
        err += dy;
        dy += 2;
      }
      if (err > 0) {
        x--;
        dx += 2;
        err += dx - (radius << 1);
      }
    }
  };
  /*
   * Iterative Bresenham
   * Misses points
   */
  var itCircle = function(radius) {
    toggled = 0;
    ctx = document.getElementById("c1").getContext("2d");
    ctx.fillText("Midpoint", 10, 20);
    for (var i = 0; i <= radius; i++) {
      toggleStyle();
      DrawCircle(i);
    }
  };
  var DrawTaxicab = function(radius) {
    for (var x = 0; x <= radius; x++) {
      for (var y = 0; y <= radius; y++) {
        if (Math.abs(x + y) == radius) {
          DrawPixel(-x, -y);
          DrawPixel(-x, +y);
          DrawPixel(+x, -y);
          DrawPixel(+x, +y);
        }
      }
    }
  };
  var itTaxicab = function(radius) {
    toggled = 0;
    ctx = document.getElementById("c2").getContext("2d");
    ctx.strokeStyle = "black";
    ctx.lineWidth = 1;
    ctx.fillText("TaxiCab / Manhattan", 10, 20);
    for (var i = 0; i <= radius; i++) {
      toggleStyle();
      DrawTaxicab(i);
    }
  };
  var DrawSquare = function(radius) {
    for (var x = 0; x <= radius; x++) {
      for (var y = 0; y <= radius; y++) {
        if (Math.abs(x) == radius || Math.abs(y) == radius) {
          DrawPixel(-x, -y);
          DrawPixel(-x, +y);
          DrawPixel(+x, -y);
          DrawPixel(+x, +y);
        }
      }
    }
  };
  var itSquare = function(radius) {
    toggled = 0;
    ctx = document.getElementById("c3").getContext("2d");
    ctx.strokeStyle = "black";
    ctx.lineWidth = 1;
    ctx.fillText("Square", 10, 20);
    for (var i = 0; i <= radius; i++) {
      toggleStyle();
      DrawSquare(i);
    }
  };
  //DrawFilledCircle(15, 15, 1);
  //DrawCircle(15, 15, 2);
  var radius = 12;
  itCircle(radius);
  itTaxicab(radius);
  itSquare(radius);